# dev-config-php

Set of configuration files for developing PHP applications.

## Installation

On your application `compser.json`, require this package:
```json
{
    "repositories": [
        {
            "url": "ssh://git@gitlab.cern.ch:7999/fence/common/dev-config-php",
            "type": "git"
        }
    ],
    "require-dev": {
        "glance-project/dev-config-php": "0.0.1"
    }
}
```

## Using with VSCode

1. Install phpcs and PHP Mess Detector extensions.
2. Add to your workspace `settings.json`
    ```json
    {
        "phpmd.rules": "/path/to/app/vendor/glance-project/dev-config-php/phpmd.xml",
        "phpcs.standard": "/path/to/app/vendor/glance-project/dev-config-php/phpcs.xml",
    }
    ```